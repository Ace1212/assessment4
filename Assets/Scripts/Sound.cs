﻿using UnityEngine;

[System.Serializable]
public class Sound
{
    // Each sound clip to be used is entered as a sounds class
    public string name; // Name of sound clip

    public AudioClip clip; // The sound clip

    [Range(0.0f, 1.0f)] // Creates a slider in Inspector to adjsut volume from 0 to 1
    public float volume; // How loud to play the clip as
    public bool loop; // Should this sound clip loop

    public AudioSource source; // Sound emitter, spawned by AudioManager as needed
}
