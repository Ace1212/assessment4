﻿using UnityEngine;

public class HomesManager : MonoBehaviour
{
    public bool isFilled; // Tracks if this home is currently full
    public bool hasFly; // Tracks if a fly is currently spawned in this home
    public bool hasWater; // Tracks if the lillypad has sunk
    public Sprite frog; // frog sprite
    public Sprite fly; // fly sprite
    public Sprite water; // water sprite
    SpriteRenderer spriteRendererChild; // Holds a reference the the sprite renderer that renders filled homes and flys

    GameManager myGameManager; //A reference to the GameManager in the scene.
    float timeUntilDespawn; // Tracks the time remaining until the fly is despawned
    int nextSpawn; // Holds whether a fly will spawn next or a lillypad will spawn next
    /// 0 = fly, 1 = water

    // Start is called before the first frame update
    void Start()
    {
        isFilled = false;
        spriteRendererChild = GetComponentInChildren<Transform>().Find("ChildSpriteRenderer").GetComponent<SpriteRenderer>(); // Find the sprite renderer that will render things in homes
        myGameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); // Find the game manager
        nextSpawn = Random.Range(0, 2); // Determine what will be spawned next in this home
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFilled && !myGameManager.gameOver && !myGameManager.isPaused) // If the frog hasn't gotten home in this home base, and the game is running
        {
            if (!hasFly && !hasWater) // does this home have a fly or no lilly pad
            {
                int spawnChance = Random.Range(0, 5001); // Generate a random number between 0 and 10000 
                if (spawnChance >= 5000) // Since this is done every frame, if the number is 9999 or higher we will spawn something
                {
                    if (nextSpawn == 0) // nextSpawn = 0 means we are spawning a fly next
                    {
                        hasFly = true; // We just spawned a fly
                        spriteRendererChild.sprite = fly; // Change sprite to the fly sprite
                    }
                    else if (nextSpawn == 1) // nextSpawn = 1 means we are despawning the lilly pad next
                    {
                        hasWater = true; // This home is now water
                        spriteRendererChild.sprite = water; // Change the sprite to water
                        spriteRendererChild.transform.localScale = new Vector2(0.65f, 0.72f); // Rescale the sprite to fit the water sprite where we need it
                    }
                    timeUntilDespawn = Random.Range(2.0f, 4.5f); // Determine how long until we should despawn the current state
                }
            }
            else if (hasFly || hasWater) // If a fly has already been spawned or lilly pad despawned
            {
                timeUntilDespawn -= Time.deltaTime; // Count down how long left until we revert to a regular home
                if (timeUntilDespawn <= 0.0f) // If countdown reaches 0
                {
                    if (hasFly) // If home has a fly, change it to not having a fly
                        hasFly = false;
                    else if (hasWater) // If home has water, change it to not having water
                    {
                        hasWater = false;
                        spriteRendererChild.transform.localScale = new Vector2(1.0f, 1.0f); // Fix sprite scaling
                    }
                    spriteRendererChild.sprite = null; // Clear the sprite
                    nextSpawn = Random.Range(0, 2); // Determine what will be spawned next in this home
                }
            }
        }
        if (isFilled) // If this home has been filled
        {
            if (hasFly) // If a fly sprite is being shown, clear it out
            {
                hasFly = !hasFly;
                spriteRendererChild.sprite = null;
            }
            else if (hasWater) // If water is being shown for some unknown reason, clear it out
            {
                hasWater = !hasWater;
                spriteRendererChild.sprite = null;
            }
            if (spriteRendererChild.sprite == null) // If the sprite is null (ie. we dont already have a frog here)
            {
                // Show a frog sprite and tag this home as a wall, which the player will treat as an obsticle on collision
                spriteRendererChild.sprite = frog;
                tag = "Wall";
            }
        }
    }
}
