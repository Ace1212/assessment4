﻿
using UnityEngine;

/// <summary>
/// This script must be used as the core Player script for managing the player character in the game.
/// </summary>
public class Player : MonoBehaviour
{
    public string playerName = ""; //The players name for the purpose of storing the high score

    [Header("Scoring")]
    public int moveForwardPoints = 10;
    public int frogHomePoints = 50;
    public int flyScoreMultiplier = 2;
    public int homesFilled; // Tracks how many frogs have gotten home

    [Header("Life and Death")]
    public int playerTotalLives; //Players total possible lives.
    public int playerLivesRemaining; //PLayers actual lives remaining.
    bool playingDeathAnim;
    float timeElapsedSinceDeath;
    public bool playerIsAlive = true; //Is the player currently alive?
    public bool playerCanMove = false; //Can the player currently move?
    bool playerShouldDie;
    public Vector2 spawnLoc;

    [Header("Movement")]
    public float hopSpeed; // Distance to move forward or backwar (the width of a lane)
    public float hopSpeedSide; // Distance to move sideways, allows for finer movement control
    public float timeMoving; // The time the current movement has taken so far
    bool movingForward; // Is the frog moving forward
    bool movingBack; // Is the frog movign backward
    bool movingLeft; // Is the frog movign left
    bool movingRight; // Is the frog moving right
    public bool isMoving; // Global movement tracking variable
    public float distanceMoved; // How far the frog has moved in the current movement, used to make sure the frog stays in the lanes
    

    private AudioManager audioManager;
    private GameManager myGameManager; //A reference to the GameManager in the scene.
    private BoxCollider2D onLog; // Houses the collider of a log the frog is on
    public Animator animator; // Reference to the frog animator
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        homesFilled = 0; // to start with, no frogs have made it home
        myGameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); // Find the game manager
        transform.position = spawnLoc; // set the player position to the starting spot
        spriteRenderer = GetComponent<SpriteRenderer>(); // Get our sprite renderer so we can manipulate it with our movement
        playerLivesRemaining = playerTotalLives; // Set the starting lives to the max lives
        audioManager = FindObjectOfType<AudioManager>(); // find the audioManager
    }

    // Update is called once per frame
    void Update()
    {
        if (!myGameManager.gameOver && myGameManager.isGameRunning)
        {
            if (homesFilled == 5) // the player has managed to get the frog safely home 5 times
            {
                // Game Won
                // Set these to false. The game manager triggers the game over state based on these both being false
                playerIsAlive = false;
                playerCanMove = false;
            }
            if (playerCanMove && playerIsAlive && !playingDeathAnim) // The main game loop. The player is alive and well
            {
                // Forward and backward raycasts of 1 hop
                RaycastHit2D hitF = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.up), 1.6f);
                RaycastHit2D hitB = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.down), 1.6f);

                Vector2 origin = new Vector2(transform.position.x - 1.7f * GetComponent<BoxCollider2D>().size.x, transform.position.y);
                RaycastHit2D hitFL = Physics2D.Raycast(origin, transform.TransformDirection(Vector2.up), 1.6f);
                RaycastHit2D hitBL = Physics2D.Raycast(origin, transform.TransformDirection(Vector2.down), 1.6f);

                origin = new Vector2(transform.position.x + 1.7f * GetComponent<BoxCollider2D>().size.x, transform.position.y);
                RaycastHit2D hitFR = Physics2D.Raycast(origin, transform.TransformDirection(Vector2.up), 1.6f);
                RaycastHit2D hitBR = Physics2D.Raycast(origin, transform.TransformDirection(Vector2.down), 1.6f);


                // If the frog falls of the log we need to know about it
                checkIfStillOnLog();

                if (!isMoving && transform.position.y < 1.5 && transform.position.y > 0)
                    transform.position = new Vector2(transform.position.x, spawnLoc.y);

                // Forward movement
                if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.E)) && transform.position.y < myGameManager.levelConstraintTop && !isMoving) // Player cannot move forward if they are already beyond the world constraints
                {
                    onLog = null;
                    if (transform.position.y >= 17) // If the player is on the last row of logs
                    {
                        if (hitF.transform.tag == "Wall") // And the object the raycast hits when they try to move forward is tagged as a wall then they havent reached an empty home
                        {
                            // Death
                            audioManager.Play("Hit");
                            playerShouldDie = true;
                        }
                        else myGameManager.currentScore += moveForwardPoints;
                    }
                    else if (transform.position.y >= 9) // If the player is in the river and doesnt hit something when they try to move forward then they fell in the river.
                    {
                        if (!hitF && !hitFL && !hitFR) // There is no log present anywhere infront of the frog
                        {
                            // Death
                            audioManager.Play("Splash");
                            playerShouldDie = true;
                        }
                        else myGameManager.currentScore += moveForwardPoints;

                    }
                    else if (!hitF && transform.position.y <= 9)
                    {
                        myGameManager.currentScore += moveForwardPoints; // If the player didn't die moving forward, reward them with 10 points. 
                        // Since the check for if there is a collision is from a raycast from the center of the player, it is possible for the raycast to not hit but the player still hit a car
                        // In this case the player still gets 10 points but also dies
                    }
                    animator.SetBool("isMoving", true);
                    movingForward = true;
                    isMoving = true;
                    audioManager.Play("Hop");
                }
                // Backward Movement
                else if ((Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.D)) && transform.position.y > myGameManager.levelConstraintBottom && !isMoving) // Player cannot move backward if they are already beyond the world constraints
                {
                    onLog = null;
                    if (transform.position.y >= 12.5) // If the player is in the river and moving backward to a row in the river and the raycast behind them isnt hitting anything, then they fell in the river
                    {
                        if (!hitB && !hitBL && !hitBR) // There is no log behind the frog
                        {
                            // Death
                            audioManager.Play("Splash");
                            playerShouldDie = true;
                        }
                    }
                    animator.SetBool("isMoving", true);
                    isMoving = true;
                    movingBack = true;
                    audioManager.Play("Hop");
                }
                // Left Movement
                else if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.S)) && transform.position.x > myGameManager.levelConstraintLeft && !isMoving) // Player must not already be beyond left world constraint
                {
                    animator.SetBool("isMoving", true);
                    isMoving = true;
                    movingLeft = true;
                    audioManager.Play("Hop");
                }
                // Right Movement
                else if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.F)) && transform.position.x < myGameManager.levelConstraintRight && !isMoving) // Player must not already be beyond right world constaint
                {
                    animator.SetBool("isMoving", true);
                    isMoving = true;
                    movingRight = true;
                    audioManager.Play("Hop");
                }
            }
            movement();
            if (!playerIsAlive) // If thew player is dead, run our code relating to respawning
            {
                timeElapsedSinceDeath += Time.deltaTime; // Count how long since player died
                if (playingDeathAnim && timeElapsedSinceDeath > 0.5) // If the player died more than half a second ago (the length of control lockout on death to prevent accidental double deaths due to player teleport to spawn
                {
                    // Handle respawning
                    animator.SetBool("isDead", false); // Cancel death animation
                    transform.position = spawnLoc; // Reset player position
                    playerIsAlive = true; // Player is alive again
                    playingDeathAnim = false; // The death animation is not being player any more
                }
            }
        }
    }
    // Moves the player
    private void movement()
    {
        if (isMoving)
        {
            if (movingForward)
            {
                spriteRenderer.flipY = true; // Make sure the frog is facing forward
                if (timeMoving < 0.25) // Move for 0.25 seconds. Gives time for movement animations without feeling like the game is slow to respond
                {
                    transform.Translate(Vector2.up * Time.deltaTime * hopSpeed * 4); // Move the frog forward at 4* their speed for 1/4 of a second so they move the right distance in a short enough time span
                    distanceMoved += Time.deltaTime * hopSpeed * 4; // Track how far we have moved
                    timeMoving += Time.deltaTime; // Track how long we have been moving for
                    // Since the top of the world is protected by collisions with walls or homes, we do not need to check if the player had somehow broken the top world constraint
                }
                else // We have been moving for at leas 0.25 seconds
                {
                    timeMoving = 0; // reset time moving
                    movingForward = false; // We nolonger are moving forward
                    if (distanceMoved != hopSpeed) // If we didn't move how far we were meant to
                    {
                        float difference = hopSpeed - distanceMoved; // Find out how far off we were
                        transform.Translate(Vector2.up * difference); // Then move the player to where they should be
                        distanceMoved = 0; // Reset movement tracker
                    }
                }
            }
            else if (movingBack)
            {
                spriteRenderer.flipY = false;
                if (timeMoving < 0.25) // Move for 0.25 seconds
                {
                    transform.Translate(-Vector2.up * Time.deltaTime * hopSpeed * 4); // Move the frog forward at 4* their speed for 1/4 of a second so they move the right distance in a short enough time span
                    distanceMoved += Time.deltaTime * hopSpeed * 4;  // Track how far we have moved
                    timeMoving += Time.deltaTime; // Track how long we have been moving for

                    if (transform.position.y < myGameManager.levelConstraintBottom) // Did we somehow end up moving outside the game world constraints?
                    {
                        transform.position = new Vector2(transform.position.x, myGameManager.levelConstraintBottom); // If so, move the play back to the edge of the world
                    }
                }
                else // We have been moving for at leas 0.25 seconds
                {
                    timeMoving = 0; // reset time moving
                    movingBack = false; // We nolonger are moving back
                    spriteRenderer.flipY = true;
                    if (distanceMoved != hopSpeed) // If we didn't move how far we were meant to
                    {
                        float difference = hopSpeed - distanceMoved; // Find out how far off we were
                        transform.Translate(-Vector2.up * difference); // Then move the player to where they should be
                        distanceMoved = 0; // Reset movement tracker
                    }
                }
            }
            else if (movingLeft)
            {
                //spriteRenderer.flipY = true; // When moving sideways we want the frog to face forward
                if (timeMoving < 0.25) // Move for 0.25 seconds
                {
                    transform.Translate(Vector2.left * Time.deltaTime * hopSpeedSide * 4); // Move the frog forward at 4* their speed for 1/4 of a second so they move the right distance in a short enough time span
                    distanceMoved += Time.deltaTime * hopSpeed * 4;
                    timeMoving += Time.deltaTime; // Track how long we have been moving for
                    
                    if (transform.position.x < myGameManager.levelConstraintLeft) // Did we somehow end up moving outside the game world constraints?
                    {
                        transform.position = new Vector2(myGameManager.levelConstraintLeft, transform.position.y); // If so, move the play back to the edge of the world
                    }
                }
                else
                {
                    timeMoving = 0; // Reset time tracker
                    movingLeft = false; // We nolonger are moving left
                    // Pinpoint accurate Sideways movement is not required, let the frog fall where it may
                    if (distanceMoved != hopSpeedSide) // If we didn't move how far we were meant to
                    {
                        float difference = hopSpeedSide - distanceMoved; // Find out how far off we were
                        transform.Translate(Vector2.left * difference); // Then move the player to where they should be
                        distanceMoved = 0; // Reset movement tracker
                    }
                }
            }
            else if (movingRight)
            {
                //spriteRenderer.flipY = true;
                if (timeMoving < 0.25) // Move for 0.25 seconds
                {
                    transform.Translate(Vector2.right * Time.deltaTime * hopSpeedSide * 4); // Move the frog forward at 4* their speed for 1/4 of a second so they move the right distance in a short enough time span
                    distanceMoved += Time.deltaTime * hopSpeed * 4;
                    timeMoving += Time.deltaTime; // Track how long we have been moving for

                    if (transform.position.x > myGameManager.levelConstraintRight) // Did we somehow end up moving outside the game world constraints?
                    {
                        transform.position = new Vector2(myGameManager.levelConstraintRight, transform.position.y); // If so, move the play back to the edge of the world
                    }
                }
                else
                {
                    timeMoving = 0; // reset time tracker
                    movingRight = false; // We nolonger are moving right
                    // Pinpoint accurate Sideways movement is not required, let the frog fall where it may
                    if (distanceMoved != hopSpeedSide) // If we didn't move how far we were meant to
                    {
                        float difference = hopSpeedSide - distanceMoved; // Find out how far off we were
                        transform.Translate(Vector2.right * difference); // Then move the player to where they should be
                        distanceMoved = 0; // Reset movement tracker
                    }
                }
            }
            if (!movingForward && !movingBack && !movingLeft && !movingRight) // If we arent moving in any direction
            {
                isMoving = false; // Then we arent moving at all
                animator.SetBool("isMoving", false); // And we should stop showing that we are moving
                if (playerShouldDie)
                    killPlayer();
            }
        }
        if (transform.position.y < myGameManager.levelConstraintBottom)
        {
            transform.position = new Vector2(transform.position.x, myGameManager.levelConstraintBottom);
        }
    }
    // Resets movement variables
    private void resetMovement()
    {
        isMoving = false;
        movingForward = false;
        movingRight = false;
        movingLeft = false;
        movingBack = false;
        distanceMoved = 0;
        timeMoving = 0;
        animator.SetBool("isMoving", false);
    }
    // Kills the player character and handles checking if the game has ended
    private void killPlayer()
    {
        resetMovement();
        playerLivesRemaining -= 1; // Decrease their lives
        animator.SetBool("isDead", true);
        playerIsAlive = false;
        playingDeathAnim = true;
        timeElapsedSinceDeath = 0;
        playerShouldDie = false;
        if (playerLivesRemaining <= 0)
        {
            playerLivesRemaining = 0; // Clamps player lives to 0
            // Game over
            playerCanMove = false;
        }
    }
    // Checks to see if the player is nolonger on a log or lillypad, such as if they moved sideway off it, or fell of by being pushed off at edge of screen
    private void checkIfStillOnLog()
    {
        bool playerDie = false;
        if (onLog != null && !isMoving) // If we actually detected an object and aren't moving
        {
            bool overlapped = GetComponent<BoxCollider2D>().Distance(onLog).isOverlapped; // Is the player overlapped with the log they think they should be?
            if (overlapped) // If they are, check by how much
            {
                float length = transform.position.x - onLog.transform.position.x;
                if (length >=  onLog.size.x + GetComponent<BoxCollider2D>().size.x) // If they are more than half off the log to the right
                {
                    // Then kill the player
                    playerDie = true;
                }
                else if (length <= -onLog.size.x - GetComponent<BoxCollider2D>().size.x) // If they are more than half off the log to the left
                {
                    // Then kill the player
                    playerDie = true;
                }
            }
            else if (!overlapped) //If the player is not overlapped with the log then they should die
            {
                playerDie = true;
            }
        }
        if (playerDie)
        {
            // Clear the reference to the log the player is on, since they arent on it anymore
            onLog = null;
            audioManager.Play("Splash");
            killPlayer();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (playerIsAlive)
        {
            Vehicle vehicle = collision.transform.parent.GetComponent<Vehicle>(); // Get a reference to the object we collided with as a Vehicle object. If it wasnt a Vehicle, then this will be null
            if (vehicle != null) // If vehicle isn't null, we hit a vehicle
            {
                if (movingBack)
                {
                    spriteRenderer.flipY = true;
                }
                if (!vehicle.isLog) // If the vehicle isnt a log, ie. is a car
                {
                    // Death
                    audioManager.Play("Hit");
                    killPlayer();
                }
                else if (vehicle.isLog) // If the vehicle was a log or lillypad
                {
                    onLog = collision.GetComponent<BoxCollider2D>(); // Store a reference to this object so we can tell if we fall off it later
                }
            }
            HomesManager home = collision.transform.gameObject.GetComponent<HomesManager>(); // Store a reference to the object hit here.
            if (home != null && home.tag == "Home") // If we actually hace a reference, check if it is tagged as "Home", the tag to denote a home base
            {
                // home.GetComponent<SpriteRenderer>().sprite = filledHome; // Swap the sprite for this home to be one with a frog in it so the player can see filled homes
                //home.tag = "Wall"; // Change the tag to "Wall". It is nolonger an empty home and "Wall" is the tag used to denote an unsafe area at the end zone of the map
                if (home.hasWater)
                {
                    audioManager.Play("Splash");
                    killPlayer();
                }
                else if (!home.hasWater)
                {
                    home.isFilled = true;
                    transform.position = new Vector2(0, 0); // Move the player back to the start so they can keep going
                    homesFilled += 1; // Incremenet our tracker of how many of the homes we have filled
                    if (home.hasFly)
                        myGameManager.currentScore += frogHomePoints * flyScoreMultiplier;
                    else
                        myGameManager.currentScore += frogHomePoints; // The player gets 50 points for getting a frog safely home
                    resetMovement();
                }
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (playerIsAlive) // While we are alive on staying within a trigger
        {
            Vehicle vehicle = collision.transform.parent.GetComponent<Vehicle>(); // Get a reference to the object we collided with as a Vehicle. Will be null if we didn't collide with a vehicle
            if (vehicle != null) // If we collided with a vehicle
            {
                if (vehicle.isLog && !movingForward && !movingBack) // Was the vehicle a log or lillypad
                {
                    if (vehicle.moveDirection == 1)
                    {
                        transform.Translate(new Vector2(vehicle.speed * Time.deltaTime, 0)); // Move the player at the log's speed in the log's direction
                        if (transform.position.x > myGameManager.levelConstraintRight) // If the player exceeds the world limits, move them back to the edge of the world
                        {
                            transform.position = new Vector2(myGameManager.levelConstraintRight, transform.position.y);
                        }
                    }
                    else if (vehicle.moveDirection == -1)
                    {
                        transform.Translate(new Vector2(-vehicle.speed * Time.deltaTime, 0)); // Move the player at the log's speed in the log's direction
                        if (transform.position.x < myGameManager.levelConstraintLeft) // If the player exceeds the world limits, move them back to the edge of the world
                        {
                            transform.position = new Vector2(myGameManager.levelConstraintLeft, transform.position.y);
                        }
                    }
                }
            }
        }
    }
}

