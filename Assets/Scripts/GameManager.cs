﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This script is to be attached to a GameObject called GameManager in the scene. It is to be used to manager the settings and overarching gameplay loop.
/// </summary>
public class GameManager : MonoBehaviour
{
    [Header("UI Elements")]
    public GameObject scoreText; // reference to text object on the UI for the score
    public GameObject highScoreText; // Reference to highScore text object
    public GameObject timeText; // Displays the time remaining in level
    public GameObject livesRemainingText; // Displays the remaining time
    public GameObject gameOverText; // Displays when the game is over with instructions
    public GameObject gameStartText; // Displays game start message
    public GameObject gamePausedText; // Displays game paused message

    [Header("Scoring")]
    public int currentScore = 0; //The current score in this round.
    static public int highScore = 0; //The highest score achieved either in this session or over the lifetime of the game.
    int allFrogsHomeBonus = 1000;
    int timeReaminingScoreMultiplier = 20;

    [Header("Playable Area")]
    public float levelConstraintTop; //The maximum positive Y value of the playable space.
    public float levelConstraintBottom; //The maximum negative Y value of the playable space.
    public float levelConstraintLeft; //The maximum negative X value of the playable space.
    public float levelConstraintRight; //The maximum positive X value of the playablle space.

    [Header("Gameplay Loop")]
    public bool isGameRunning; //Is the gameplay part of the game current active?
    public float totalGameTime; //The maximum amount of time or the total time avilable to the player.
    public float gameTimeRemaining; //The current elapsed time
    public bool gameOver; // Has the game ended
    public bool isPaused;

    // Reference to the player
    public Player player;
    AudioManager audioManager;
    bool audioSwitched;
    bool gameBeginPause;

    // Start is called before the first frame update
    void Start()
    {
        currentScore = 0;
        // Start the game running
        isGameRunning = true;
        gameOver = false; // The game is not over
        // Initialise the time to the length of the game
        gameTimeRemaining = totalGameTime;

        // Find all the UI elements
        scoreText = GameObject.Find("ScoreDisplay"); // Find the score text element
        highScoreText = GameObject.Find("HighScoreDisplay"); // Find the highScore text element
        timeText = GameObject.Find("TimeDisplay"); // Find the time text element
        livesRemainingText = GameObject.Find("LivesRemainingDisplay"); // Find the lives text element
        gameOverText = GameObject.Find("GameOverBackground"); // find the gameOver text element
        gameOverText.SetActive(false); // Disbale the gameOver UI element
        gamePausedText = GameObject.Find("GamePausedBackground"); // Find the game paused UI element
        gamePausedText.SetActive(false); // Disable the game over UI element
        gameStartText = GameObject.Find("StartGameBackground"); // Find the game start UI element
        audioManager = FindObjectOfType<AudioManager>(); // Find the audio manager

        // If a high score has been saved
        if (PlayerPrefs.HasKey("highScore"))
        {
            if (PlayerPrefs.GetInt("highScore") > highScore) // and if that high score is higher than any high score that might be loaded locally
            {
                highScore = PlayerPrefs.GetInt("highScore"); // load the saved high score into local
            }
        }
        audioManager.Play("Start"); // Cue the intro music
        gameBeginPause = true; // Set initial game state
        pauseGame(); // Pause the game
    }

    // Update is called once per frame
    void Update()
    {
        // The game is running normally and is currently in progress
        if (isGameRunning && !gameOver) 
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.P)) //Press Space or P to pause
            {
                pauseGame();
            }
            else if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Q)) // Press Escape or Q to quit
            {
                quitGame();
            }
        }
        // The game is not running normally but is currently in progress
        else if (!isGameRunning && !gameOver) 
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.P)) // Press Space or P to unpause
            {
                unpauseGame();
            }
        }
        // When time runs out the game should stop running and is over
        if (!gameOver && gameTimeRemaining <= 0) 
        {
            endGame();
        }
        // If the player is dead and cant move then the game is over and should stop running
        if (!player.playerIsAlive && !player.playerCanMove && !gameOver) 
        {
            endGame();
        }
        // The game is not running and is not in progress
        if (gameOver) 
        {
            gameOverText.SetActive(true); // Display the game over text
            if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Return)) // Press R or Enter to start a new game
            {
                reloadGame();
            }
        }

        // Display UI elements
        displayUI();

        if (gameTimeRemaining <= 5 && !audioSwitched) // If there is 5 seconds or less left in the game and we havent switched on the time low clip
        {
            // Turn on the time low audio clip
            audioSwitched = true;
            audioManager.Play("Time");
        }
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Escape)) // Press Escape or Q to quit
        {
            quitGame();
        }
    }
    // Reloads the scene so the game can be replayed
    public void reloadGame()
    {
        // Reload the game
        SceneManager.LoadScene(0);
    }
    // Called whenever a game ends, updates any game end score bonuses, handles game ending and checks if there is a new high score
    public void endGame()
    {
        gameOver = true; // The game is over
        audioManager.Stop("Time"); // Stop playing the low time audio clip
        if (gameTimeRemaining < 0) // If the time has overshot 0 seconds due to frame rates
        {
            gameTimeRemaining = 0.00f; // Time.deltaTime might slightly overshoot 0 seconds due to its nature, this pulls the time back to 0 to display to user
        }
        if (player.homesFilled == 5) // If the player won the game by getting all 5 frogs home then they get a score bonus based on the time remaining
        {
            audioManager.Play("Win"); // Play game win audio clip
            currentScore += allFrogsHomeBonus; // bonus points for getting all the frogs home
            currentScore = currentScore + (int)(gameTimeRemaining) * timeReaminingScoreMultiplier; // Award time reamining bonus points
        }
        else // Otherwise the game was lost
        {
            // No bonus points awarded for losing.
            audioManager.Play("Loss"); // Play game lost audio clip
        }
        if (currentScore > highScore) // If the current score for the game is better than the local high score, overwrite the high score then save the new high score
        {
            highScore = currentScore; // update the highscore
            // Save Data
            PlayerPrefs.SetInt("highScore", highScore);
        }
        pauseGame(); // Pause the game since the game is over
    }
    // Quits the game
    public void quitGame()
    {
        // Save data
        PlayerPrefs.SetInt("highScore", highScore);
        // Quits game in unity editor
        //UnityEditor.EditorApplication.isPlaying = false;
        // Quits game when built
        Application.Quit();
    }
    // Pauses gameplay
    private void pauseGame()
    {
        if (gameTimeRemaining <= 5) // If there is less than 5 seconds left, then the low time sound clip is playing.
            audioManager.Stop("Time"); // stop that clip while the game is paused
        if (!gameBeginPause && !gameOver) // We have a different UI element for the pre-game pause
            gamePausedText.SetActive(true); // Display the game paused UI element
        // Sets timescale to 0 and adjust variable to track game state
        Time.timeScale = 0;
        isPaused = true;
        isGameRunning = false;
    }
    // Resumes gameplay
    private void unpauseGame()
    {
        if (gameBeginPause) // If we are currently in the pre-game pause
        {
            gameStartText.SetActive(false); // turn off the game start text
            audioManager.Stop("Start"); // If the Start music is playing, cancel it
            audioManager.Play("Theme"); // Start playing the theme song
            gameBeginPause = false; // update the game state variable, the pre-game pause is ended
        }
        else // The game paused UI element only displays when the game is paused while the game is active
        {
            gamePausedText.SetActive(false); // Disable the game paused UI element
        }

        if (gameTimeRemaining <= 5) // If there is less than 5 seconds left, the low time clip should be playing
            audioManager.Play("Time"); // So make it play
        // sets timescale to 1 and asjusts variables to track game state
        isPaused = false;
        isGameRunning = true;
        Time.timeScale = 1;
    }
    // Calls all UI display functions
    private void displayUI()
    {
        displayScore();
        displayTime();
        displayLives();
    }
    // Updates and displays the current score and displays the local high score
    private void displayScore()
    {
        //currentScore = player.score;
        scoreText.GetComponent<Text>().text = "Score: " + currentScore;
        highScoreText.GetComponent<Text>().text = "High Score: " + highScore;
    }
    // Updates and displays the time remaining text
    private void displayTime()
    {
        if (!gameOver && !gameBeginPause) // If the game isnt over and has started
            gameTimeRemaining -= Time.deltaTime; // Updates the time remaining
        float timeR = (float)Math.Round(gameTimeRemaining * 100f) / 100f; // Rounds time remaining to 2 decimal places
        timeText.GetComponent<Text>().text = "Time: " + timeR; // Displays the rounded time
    }
    // Updates and displays the lives remaining text
    private void displayLives()
    {
        livesRemainingText.GetComponent<Text>().text = "Lives: " + player.playerLivesRemaining;
    }
}
