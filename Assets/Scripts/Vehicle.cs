﻿using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    /// <summary>
    /// -1 = left, 1 = right
    /// </summary>
    public int moveDirection = 0; //This variabe is to be used to indicate the direction the vehicle is moving in.
    public float speed; //This variable is to be used to control the speed of the vehicle.
    public Vector2 startingPosition; //This variable is to be used to indicate where on the map the vehicle starts (or spawns)
    public Vector2 endPosition; //This variablle is to be used to indicate the final destination of the vehicle.
    public List<GameObject> vehicles; // List of all child vehicles
    public List<GameObject> vehiclePrefabs; // List of available vehicles to spawn
    public GameObject myParticleSystem; // Reference to the particle system to be used
    public Vector2 spawnSpeed; // Vector containing the range of time in seconds to delay before spawning the next vehicle. NOTE: x should be > y but if it isn't they will be swapped in Start()
    Vector2 vehiclePos; // Position to spawn a vehicle at
    public float timeSinceLastSpawn; // Time since the last vehicle was spawned
    public GameObject ParentGameObject;
    public float timeUntilNextSpawn; // holds randiom value in spawnSpeed range to wait until next spawn
    public bool isLog; // If the vehicle's are to be logs and lillypads on the river this is true

    

    // Start is called before the first frame update
    void Start()
    {
        // Initialise the time since the last vehicle was spawned to 0 seconds
        timeSinceLastSpawn = 0.0f;
        if (moveDirection == 1) // Default configuration of variables is for moveDirection = -1, if moveDirection is instead 1, we need to change some things
        {
            // Swap the starting and ending positions
            Vector2 temp = startingPosition;
            startingPosition = endPosition;
            endPosition = temp;
            
        }   
        vehiclePos = startingPosition; // We want to spawn the vehicle at its starting position

        for (int i = 0; i < 2; i++) // Initially spawn 2 vehicles in our lane with random offsets
        {
            spawn();
            float offset = Random.Range(-1.5f, 1.5f);
            if (i == 0) vehiclePos.x = -5 + offset;
            else if (i == 1) vehiclePos.x = 5 + offset;
            vehicles[i].transform.position = vehiclePos;
        }
        spawn();

        if (moveDirection == 1) // If we are not moving in the default direction we need to flip the sprite around otherwise everything will be moving backwards
        {
            for (int i = 0; i < vehicles.Count; i++)
            {
                if (vehicles[i].GetComponent<SpriteRenderer>())
                vehicles[i].GetComponent<SpriteRenderer>().flipX = false;
            }
        }
        if (spawnSpeed.y > spawnSpeed.x)
        {
            float temp = spawnSpeed.x;
            spawnSpeed.x = spawnSpeed.y;
            spawnSpeed.y = temp;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawn += Time.deltaTime; // Increment elapsed time since last spawn
        for (int i = 0; i < vehicles.Count; i++) // Check to see if any vehicles have reached their ending position, and destory them if they have
        {
            if (moveDirection == -1)
            {
                if (vehicles[i].transform.position.x <= endPosition.x)
                {
                    Destroy(vehicles[i]); // Destroy the vehicle
                    vehicles.Remove(vehicles[i]); // Then remove our reference to it from our list of vehicles
                }
            }
            else if (moveDirection == 1)
            {
                if (vehicles[i].transform.position.x >= endPosition.x)
                {
                    Destroy(vehicles[i]); // Destroy the vehicle
                    vehicles.Remove(vehicles[i]); // Then remove our reference to it from our list of vehicles
                }
            }
        }
        for (int i = 0; i < vehicles.Count; i++) // Move all the vehicles at their speed in their direction
        {
            vehicles[i].transform.Translate(Vector2.right * Time.deltaTime * speed * moveDirection);
        }

        if (timeSinceLastSpawn >= timeUntilNextSpawn) // If enough time has elapsed since the last spawn
        {
            spawn(); // Spawn a new vehicle
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Handle collisions of vehicles and frog
    }

    void spawn()
    {
        var j = Random.Range(0, vehiclePrefabs.Count); // Randomly determine which vehicle prefab should be used
        GameObject vehicle = Instantiate(vehiclePrefabs[j], transform); // Instantiate a copy of the chosen prefab
        vehicle.transform.position = startingPosition; // Set the new vehicle to its starting position
        if (moveDirection == 1 && vehicle.GetComponent<SpriteRenderer>()) // If we arent moving in the default direction, flip the sprite so it isn't moving backward
        {
            vehicle.GetComponent<SpriteRenderer>().flipX = false;
        }
    
        GameObject particleSystem = Instantiate(myParticleSystem); // Instantiate our particle system
        if (isLog) // If this vehicle is in the river (ie. is a log ro lillypad)
                particleSystem.GetComponent<ParticleSystem>().startColor = new Color(0.15f, 0.43f, 0.48f); // change its colour to a blue so it looks like splashes
        particleSystem.transform.parent = vehicle.transform; // Set it to be a child of the new vehicle
        float vehicleLength = vehicle.GetComponent<BoxCollider2D>().size.x; // retrieve the vehicle's length so we know how much to offset the paricle system
        if (moveDirection == -1) // Moving left
        {            
            particleSystem.transform.position = new Vector2(vehicle.transform.position.x + vehicleLength, vehicle.transform.position.y); // Offset the particle system to the right
            particleSystem.transform.rotation = new Quaternion(0f, 90f, 0f, 1f); // Turn the particle system so it shoots out behind the vehicle (to the right)
        }
        else
        {
            particleSystem.transform.position = new Vector2(vehicle.transform.position.x - vehicleLength, vehicle.transform.position.y); // Offset the particle system to the left
            particleSystem.transform.rotation = new Quaternion(0f, -90f, 0f, 1f); // Turn the particle system so it shoots out behind the vehicle (to the left)
        }
        vehicles.Add(vehicle); // Add the vehicle to our list of currently spawned vehicles

        timeSinceLastSpawn = 0; // reset elapsed time
        timeUntilNextSpawn = Random.Range(spawnSpeed.x, spawnSpeed.y); // Determine a new amount of time to wait
    }
}
