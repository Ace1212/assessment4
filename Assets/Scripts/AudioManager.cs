﻿using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (Sound s in sounds) // For each sound clip
        {
            s.source = gameObject.AddComponent<AudioSource>();  // Add an audio source
            s.source.clip = s.clip; // Assign the sound clip

            s.source.volume = s.volume; // Set its volume from the provided sound
            s.source.pitch = 1.0f; // Set the pitch to 1, could be made adjustable like volume but isn't needed here
            s.source.loop = s.loop; // Set if the clip should loop or not
        }
    }

    public void Play (string name) // Plays the audio clip with the passed name
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); // Seach the array of sounds for the given clip
        if (s != null)
            s.source.Play(); // If the clip is found play it
        // If the clip isn't found, do nothing
    }
    public void Stop(string name) // Stops playing the audio clip with the passed name
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); // Search the array of sounds for the given clip
        if (s != null)
        {
            s.source.Stop(); // If the clip is found, stop playing it
        }
        // If the clip isn't found, do nothing
    }
}
