Project Management:
	Source control on Bitbucket @ https://bitbucket.org/Ace1212/assessment4/

Game Instructions:
	Up or E:	Move forward
	Down or D:	Move backward
	Left or S:	Move left
	Right or F:	move right
	R or Enter:	Restart the game once game has ended
	Q or Esc:	Quit the game at any time
	Space or P:	Pause/Unpause game

Changelog:
	v5.3
	Adjusted particle system colour for splashes in river to make them more visible on the new river sprite

	v5.2
	Fixed some UI display bugs where before game began time displayed as having already started ticking down
		and after game ended it displayed as being less than 0 seconds remaining
	Added a pause UI display to clearly indicate that the game is paused and provide unpause instructions

	v5.1
	Reworked patch that prevents lane alignment loss on respawn due to reported errors resulting in death on respawn in certain situations
		NB: I was unable to reproduce this issue so this is only a best guess on problem. It cleans up the system either way
	
	v5.0
	Reworked how getting a frog home works to support expanded functionality in homes
	Added random fly spawns to home bases. Getting to a home base with a fly in it is worth double points
	Lilly pads may instead disappear from homes rather than a fly spawning. Landing in a home without a lilly pad will kill the frog
	Minor bug fixes in movement and player reseting on death
	Added game begin screen with game instructions and title
	Game no longer immediately begins on launch
	Cleaned up music transitions at begining of game
	Adjusted sound effect volumes
	Increased the precision of sideways player movement to make movement more consistent and predictable
	Adjusted speed and spawn rates of problem vehicles and logs. Some logs required long stretches of waiting on them moving at slow speeds
	Changed water zone background sprite to match the background around the lilly pads in the home area
	Reworked death registering to correct wierd movement during death animation when dying in water
	Adjusted score to be solely tracked by the game manager instead of weirdly split between the player and game manager
		This corrected several issues with display and registering of score and new high scores at game end
	Corrected an issue that caused death animation to play upside down if the player died while moving back toward the start
	Expanded game quit functionality to work at any time in the game
	If player loses alignment with the lanes due to spaming movement while respawning, moving after respawn should now restore
		correct lane aligmnet. Lane aligmnet loss should not be enough to cause collisions with vehicles from only one occurance,
		instead taking multiple occurances of the bug to cause an issue, which should now be impossible

	v4.1
	Corrected issues with death animation and reinstated it
	Updated game over UI element to make it more readable

	v4.0
	Added particle systems to logs and lilly pads
	Added secondary controls
	Added frog movement animations
	Reworked movement system into movement over time rather than instantaneous teleportation
	Added slight pause on death to help prevent accidental movement on respawn
	Added sounds and music
	Cleaned up detection for if the frog is on a log or lillypad. They must now be at least half on or they die.

	v3.0
	Added Score tracking and display
	Added Time tracking and display
	Added Lives tracking and display
	Added Game Over popup
	Added ability to pause and resume game
	Added game ending logic
	Added game quit logic
	Added ability to start new game
	Added high score tracking within a single launch of the application
	Saves high score between sessions

	v2.0
	Added ability to get frogs home
	Player detects if moving onto a log or lillypad when moving in river and if they fall off of one

	v1.0
	Basic player movement
	Car + Truck and Log + Lillypad spawners
	Car and Truck collisions with player
	Player moved with Logs and Lillypads, death for falling in water or off objects
	Updated background to be formed of components rather than a screenshot

	v0.1
	Exploratory simple vehilces spawner made by moving them back and forth
	Frogger Screenshot as sample background

Attribution:
	Frogger art assets obtained from https://www.clipartkey.com/view/xxboTb_frogger-sprite-sheet/
	Sound effects obtained from http://www.classicgaming.cc/classics/frogger/sounds
		Most sound files include attribution documents
	Theme music and start tune obtained from http://www.digitpress.com/
	Game Over sound obtained from http://www.orangefreesounds.com/
		Includes attribution and license document
	Made with reference to coding examples from GPR103 lectures
	audioManager and Sound scripts made with reference to tutorial by Brackeys at https://www.youtube.com/watch?v=6OT43pvUyfY

Technical Requirements:
	None